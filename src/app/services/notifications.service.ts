import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { AuthService } from "../auth.service";

@Injectable({
  providedIn: "root"
})
export class NotificationsService {
  API = environment.api;
  constructor(private http: HttpClient, private auth: AuthService) {}
  getNotifications() {
    let httpOptions = {
      headers: new HttpHeaders({
        authorization: localStorage.getItem("_token")
      })
    };
    let data = {
      handle: "get_notification"
    };
    return this.http.post(this.API, JSON.stringify(data), httpOptions);
  }
  read_all() {
    let httpOptions = {
      headers: new HttpHeaders({
        authorization: localStorage.getItem("_token")
      })
    };
    let data = {
      handle: "read_seller_notification"
    };
    return this.http.post(this.API, JSON.stringify(data), httpOptions);
  }

  get_notification_count() {
    let httpOptions = {
      headers: new HttpHeaders({
        authorization: localStorage.getItem("_token")
      })
    };
    let data = {
      handle: "get_notification_count"
    };
    return this.http.post(this.API, JSON.stringify(data), httpOptions);
  }
}

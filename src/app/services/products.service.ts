import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { AuthService } from "../auth.service";

@Injectable({
  providedIn: "root"
})
export class ProductsService {
  API = environment.api;
  constructor(private http: HttpClient, private auth: AuthService) {}
  getproducts() {
    let httpOptions = {
      headers: new HttpHeaders({
        authorization: localStorage.getItem("_token")
      })
    };
    let data = {
      handle: "get_products"
    };
    return this.http.post(this.API, JSON.stringify(data), httpOptions);
  }
  getProduct(id) {
    let httpOptions = {
      headers: new HttpHeaders({
        authorization: localStorage.getItem("_token")
      })
    };
    let data = {
      handle: "get_product",
      id: id
    };
    return this.http.post(this.API, JSON.stringify(data), httpOptions);
  }

  add_order_req(data) {
    let httpOptions = {
      headers: new HttpHeaders({
        authorization: localStorage.getItem("_token")
      })
    };
    return this.http.post(this.API, JSON.stringify(data), httpOptions);
  }
}

import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";

import { environment } from "../../environments/environment";
const httpOptions = {
  headers: new HttpHeaders({
    authorization: localStorage.getItem("_token")
  })
};
@Injectable({
  providedIn: "root"
})
export class OrderRequestsService {
  API = environment.api;
  constructor(private http: HttpClient) {}

  public GetOrderRequests() {
    let data = {
      handle: "order_requests"
    };
    return this.http.post(this.API, JSON.stringify(data), httpOptions);
  }

  public cancel_request(id) {
    let data = {
      handle: "cancel_req",
      req_id: id
    };
    return this.http.post(this.API, JSON.stringify(data), httpOptions);
  }
}

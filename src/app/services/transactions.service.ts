import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class TransactionsService {
  API = environment.api;
  constructor(private http: HttpClient) {}
  getTransactionDetails() {
    let httpOptions = {
      headers: new HttpHeaders({
        authorization: localStorage.getItem("_token")
      })
    };
    let data = {
      handle: "get_transaction_details"
    };
    return this.http.post(this.API, JSON.stringify(data), httpOptions);
  }
}

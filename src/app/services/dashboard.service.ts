import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
const httpOptions = {
  headers: new HttpHeaders({
    authorization: localStorage.getItem("_token")
  })
};
@Injectable({
  providedIn: "root"
})
export class DashboardService {
  API = environment.api;
  constructor(private http: HttpClient) {}

  public details() {
    let data = {
      handle: "dashboard_details"
    };
    return this.http.post(this.API, JSON.stringify(data), httpOptions);
  }
}

import { TestBed } from '@angular/core/testing';

import { OrderRequestsService } from './order-requests.service';

describe('OrderRequestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OrderRequestsService = TestBed.get(OrderRequestsService);
    expect(service).toBeTruthy();
  });
});

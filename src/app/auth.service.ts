import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";
import PNotify from "pnotify/dist/es/PNotify";
import PNotifyButtons from "pnotify/dist/es/PNotifyButtons";
import { environment } from "../environments/environment";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  httpOptions = {
    headers: new HttpHeaders({
      authorization: localStorage.getItem("_token")
    })
  };
  public isLoggedin: boolean = false;
  public API = environment.api;
  constructor(private http: HttpClient, private router: Router) {}

  GetUserDetails(data) {
    data.handle = "login";
    this.http.post(this.API, JSON.stringify(data)).subscribe(
      response => {
        localStorage.setItem("_token", response["token"]);
        localStorage.setItem("isLoggedin", "true");
        this.setLoggedin(true);
        this.ShowMessage(response["message"], "success");
        this.router.navigate(["/dashboard"]);
      },
      err => {
        localStorage.setItem("isLoggedin", "false");
        this.setLoggedin(false);
        this.ShowMessage(err.error.message, "error");
      }
    );
  }

  ShowMessage(message, type) {
    PNotifyButtons;
    PNotify.defaults.styling = "bootstrap3";
    PNotify.alert({
      icon: false,
      text: message,
      type: type
    });
  }

  setLoggedin(value: boolean) {
    this.isLoggedin = value;
  }

  get isLoggedInCheck() {
    if (localStorage.getItem("isLoggedin") == "true") {
      return true;
    } else {
      return false;
    }
  }

  error_handler(err) {
    if (err.status == 401) {
      localStorage.setItem("_token", "");
      localStorage.setItem("isLoggedin", "false");
      this.router.navigate(["/login"]);
    }
  }

  response_handler(res) {
    let responses = res["message"];
    for (var key in responses) {
      if (responses.hasOwnProperty(key)) {
        this.ShowMessage(responses[key], res["type"]);
      }
    }
  }

  verify_token() {
    var data = {
      handle: "check_Auth"
    };
    this.http.post(this.API, JSON.stringify(data), this.httpOptions).subscribe(
      response => {
        localStorage.setItem("isLoggedin", "true");
      },
      err => {
        if (err.error.status == false) {
          localStorage.setItem("_token", "");
          localStorage.setItem("isLoggedin", "false");
          this.router.navigate(["/login"]);
        }
      }
    );
  }
}

import { Component } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AuthService } from "../../auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-dashboard",
  templateUrl: "login.component.html"
})
export class LoginComponent {
  constructor(private auth: AuthService, private route: Router) {}

  ngOnInit(): void {
    this.auth.verify_token();

    if (this.auth.isLoggedInCheck == true) {
      console.log("login thay javu");
      this.route.navigate(["/dashboard"]);
    }
  }

  form = new FormGroup({
    username: new FormControl("", [
      Validators.required,
      Validators.minLength(3)
    ]),
    password: new FormControl("", Validators.required)
  });

  get username() {
    return this.form.get("username");
  }

  onSubmit() {
    this.auth.GetUserDetails(this.form.value);
    // TODO: Use EventEmitter with form value
  }
}

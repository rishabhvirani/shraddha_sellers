import { Component, ViewChild, OnInit } from "@angular/core";
import { ProductsService } from "../../services/products.service";
import { OrderNowComponent } from "../component/order-now/order-now.component";
import { AuthService } from "../../auth.service";
import { ModalOptions, ModalDirective } from "ngx-bootstrap/modal";

@Component({
  templateUrl: "./products.component.html",
  styleUrls: ["./products.component.scss"]
})
export class ProductsComponent implements OnInit {
  @ViewChild(OrderNowComponent, { static: true }) OrderNow: OrderNowComponent;

  dtOptions: DataTables.Settings = {
    pagingType: "full_numbers",
    processing: true
  };
  products = [];

  constructor(private pservice: ProductsService, private auth: AuthService) {}
  ngOnInit() {
    this.pservice.getproducts().subscribe(
      response => {
        this.products = response["result"]["products"];
      },
      err => {
        this.auth.error_handler(err);
      }
    );
  }

  order_product(id) {
    this.OrderNow.ShowOrderBox(id);
  }
}

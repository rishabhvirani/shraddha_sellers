// Angular
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgModule } from "@angular/core";
import { ProductRoutingModule } from "./products-routing.module";
import { ProductsComponent } from "./products.component";
import { DataTablesModule } from "angular-datatables";
import { ModalModule } from "ngx-bootstrap/modal";
import { OrderNowComponent } from "../component/order-now/order-now.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ModalModule.forRoot(),
    ProductRoutingModule,
    DataTablesModule,
    ReactiveFormsModule
  ],
  declarations: [ProductsComponent, OrderNowComponent]
})
export class ProductsModule {}

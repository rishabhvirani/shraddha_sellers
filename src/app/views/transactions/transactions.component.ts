import { Component, OnInit } from "@angular/core";
import { TransactionsService } from "../../services/transactions.service";
import { AuthService } from "../../auth.service";

class transaction {
  amount: number;
  closing_balance: number;
  created_at: string;
  label: number;
  pc_name: string;
  product_name: string;
  qty: number;
  seller_rate: number;
  total_amount: number;
}

@Component({
  templateUrl: "./transactions.component.html",
  styleUrls: ["./transactions.component.scss"]
})
export class TransactionsComponent implements OnInit {
  transactions: transaction;
  balance: number = 0;
  constructor(
    private transaction: TransactionsService,
    private auth: AuthService
  ) {
    this.transaction.getTransactionDetails().subscribe(
      res => {
        this.transactions = res["result"]["transactions"];
        this.balance = res["balance"];
      },
      err => {
        this.auth.error_handler(err);
      }
    );
  }
  ngOnInit() {}
}

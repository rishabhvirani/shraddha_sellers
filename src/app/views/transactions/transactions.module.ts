// Angular
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { TransactionsRoutingModule } from "./transactions-routing.module";
import { DataTablesModule } from "angular-datatables";
import { ModalModule } from "ngx-bootstrap/modal";
import { TransactionsComponent } from "./transactions.component";

@NgModule({
  imports: [
    CommonModule,
    ModalModule.forRoot(),
    TransactionsRoutingModule,
    DataTablesModule
  ],
  declarations: [TransactionsComponent]
})
export class TransactionModule {}

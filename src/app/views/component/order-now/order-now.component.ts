import { Component, OnInit, Input, ViewChild } from "@angular/core";
import {
  ModalDirective,
  ModalOptions,
  BsModalService
} from "ngx-bootstrap/modal";
import { ProductsService } from "../../../services/products.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AuthService } from "../../../auth.service";

class Product {
  image_Status: number;
  image_count: number;
  pc_name: string;
  pc_tax: number;
  pc_type: number;
  product_id: number;
  product_images: string | null;
  product_name: string | null;
  product_rate: number;
  seller_rate: number;
  uom_name: string;
  uom_qty: number;
}

class OrderReq {
  total: number;
  qty: number;
}

@Component({
  selector: "app-order-now",
  templateUrl: "./order-now.component.html",
  styleUrls: ["./order-now.component.scss"]
})
export class OrderNowComponent implements OnInit {
  product: Product;
  qty_val: number;
  total: number;
  button_status: boolean = true;

  @Input() product_detail: string;
  @ViewChild("largeModal", { static: false }) public largeModal: ModalDirective;

  constructor(private pservice: ProductsService, private auth: AuthService) {}

  ngOnInit() {}

  order_form = new FormGroup({
    qty: new FormControl("", [Validators.required])
  });

  ShowOrderBox(id) {
    this.qty_val = 0;
    this.total = 0;
    this.pservice.getProduct(id).subscribe(
      res => {
        this.product = res["result"]["product"];
        if (/[,-]/.test(this.product.product_images) == true) {
          this.product.product_images =
            "http://localhost/shraddhap/" +
            this.product.product_images.split(",")[0];
        } else if (this.product.product_images == null) {
          this.product.product_images =
            "http://localhost:4200/assets/no-image.png";
        } else {
          this.product.product_images =
            "http://localhost/shraddhap/" + this.product.product_images;
        }
        this.largeModal.show();
      },
      err => {
        this.auth.error_handler(err);
      }
    );
  }

  calculate() {
    this.qty_val = Math.round(Math.abs(this.order_form.get("qty").value));
    this.total = this.qty_val * this.product.seller_rate;
  }

  onSubmit() {
    this.button_status = false;
    let data = {
      handle: "add_request",
      product_id: this.product.product_id,
      qty: this.order_form.value.qty,
      seller_rate: this.product.seller_rate,
      total_amount: this.total
    };

    this.pservice.add_order_req(data).subscribe(
      res => {
        this.button_status = true;
        this.auth.response_handler(res);
        if (res["status"] == true) {
          this.largeModal.hide();
        }
      },
      err => {
        this.button_status = true;
        this.auth.error_handler(err);
      }
    );
  }
}

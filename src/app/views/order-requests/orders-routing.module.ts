import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { OrderRequestsComponent } from "./order-requests.component";

const routes: Routes = [
  {
    path: "",
    component: OrderRequestsComponent,
    data: {
      title: "Order Requests"
    }
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderReqRoutingModule {}

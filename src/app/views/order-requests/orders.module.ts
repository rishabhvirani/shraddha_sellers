// Angular
import { CommonModule } from "@angular/common";

import { NgModule } from "@angular/core";
import { DataTablesModule } from "angular-datatables";

import { OrderReqRoutingModule } from "./orders-routing.module";
import { OrderRequestsComponent } from "./order-requests.component";

@NgModule({
  imports: [CommonModule, OrderReqRoutingModule, DataTablesModule],
  declarations: [OrderRequestsComponent]
})
export class OrdersModule {}

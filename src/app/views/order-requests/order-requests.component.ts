import { Component, OnInit } from "@angular/core";
import { OrderRequestsService } from "../../services/order-requests.service";
import { AuthService } from "../../auth.service";

class order_req {
  product_name: string;
  pc_name: string;
  qty: number;
  seller_rate: number;
  total_amount: number;
  created_at: string;
  req_status: number;
  req_id: number;
  isDisabled: boolean = false;
  isloading: boolean = false;
}

@Component({
  templateUrl: "./order-requests.component.html",
  styleUrls: ["./order-requests.component.scss"]
})
export class OrderRequestsComponent implements OnInit {
  order_requests: order_req;
  dtOptions: DataTables.Settings = {};

  constructor(
    private orderreq: OrderRequestsService,
    private auth: AuthService
  ) {
    this.dtOptions = {
      pagingType: "full_numbers"
    };

    this.orderreq.GetOrderRequests().subscribe(
      res => {
        this.order_requests = res["result"]["order_requests"];
      },
      err => {
        auth.error_handler(err);
      }
    );
  }

  ngOnInit() {}

  cancel_req(req) {
    req.isloading = true;
    this.orderreq.cancel_request(req.req_id).subscribe(
      res => {
        this.auth.response_handler(res);

        res["req_status"] == 0
          ? (req.isDisabled = false)
          : (req.isDisabled = true);
        req.req_status = res["req_status"];
        req.isloading = false;
      },
      err => {
        this.auth.error_handler(err);
      }
    );
  }
}

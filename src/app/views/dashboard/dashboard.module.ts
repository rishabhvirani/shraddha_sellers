import { NgModule } from "@angular/core";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { DashboardComponent } from "./dashboard.component";
import { DashboardRoutingModule } from "./dashboard-routing.module";

@NgModule({
  imports: [DashboardRoutingModule, BsDropdownModule],
  declarations: [DashboardComponent]
})
export class DashboardModule {}

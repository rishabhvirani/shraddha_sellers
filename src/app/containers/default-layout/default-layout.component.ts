import { Component, OnDestroy, Inject } from "@angular/core";
import { DOCUMENT } from "@angular/common";
import { navItems } from "../../_nav";
import { Router, NavigationEnd } from "@angular/router";
import { NotificationsService } from "../../services/notifications.service";
import { AuthService } from "../../auth.service";
class notification {
  seen: number;
  req_status: number;
  proudct_name: string;
}
@Component({
  selector: "app-dashboard",
  templateUrl: "./default-layout.component.html",
  styleUrls: ["./default.scss"]
})
export class DefaultLayoutComponent implements OnDestroy {
  public notifications: notification;
  public unseen: number;
  public env_label: number = 0;
  public navItems = navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement;
  constructor(
    private route: Router,
    private auth: AuthService,
    private notification: NotificationsService,
    @Inject(DOCUMENT) _document?: any
  ) {
    this.route.events.subscribe(res => {
      console.log(res);
      if (res instanceof NavigationEnd) {
        this.get_notification_count();
      }
    });
    this.changes = new MutationObserver(mutations => {
      this.sidebarMinimized = _document.body.classList.contains(
        "sidebar-minimized"
      );
    });
    this.element = _document.body;
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ["class"]
    });
    this.get_notification_count();
  }

  public read_all() {
    this.notification.read_all().subscribe(
      res => {
        this.env_label = 0;
      },
      err => {
        this.auth.error_handler(err);
      }
    );
  }

  public get_notification_count() {
    this.notification.get_notification_count().subscribe(
      res => {
        res["status"] == true
          ? ((this.unseen = res["unseen_count"]),
            (this.env_label = res["unseen_count"]))
          : (this.env_label = 0);
      },
      err => {
        this.auth.error_handler(err);
      }
    );
  }

  public get_notifications() {
    this.notification.getNotifications().subscribe(
      response => {
        this.notifications = response["result"]["notifications"];
        this.read_all();
      },
      err => {
        this.auth.error_handler(err);
      }
    );
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }

  logout() {
    localStorage.setItem("isLoggedin", "false");
    localStorage.setItem("_token", "");
    this.route.navigate(["/login"]);
  }
}
